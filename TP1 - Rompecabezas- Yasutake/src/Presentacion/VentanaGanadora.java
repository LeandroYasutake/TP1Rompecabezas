package Presentacion;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;

public class VentanaGanadora extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private ImagePanel panelFondo;
	private JLabel hasGanado;
	private JLabel labelMovimientos;
	public JLabel cantidadMovimientos;
	private JButton aceptar;
	public boolean yaSeMostro;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VentanaGanadora dialog = new VentanaGanadora();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaGanadora() 
	{
		this.yaSeMostro = false;
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		inicializar();
	}

	public boolean getYaSeMostro() {
		return yaSeMostro;
	}

	public void setYaSeMostro(boolean yaSeMostro) {
		this.yaSeMostro = yaSeMostro;
	}

	private void inicializar() 
	{
		setBounds(100, 100, 330, 185);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		contentPanel.setLayout(null);
		
		hasGanado = new JLabel();
		hasGanado.setIcon(new ImageIcon("Imagenes\\ventana\\labelGanador.png"));;
		hasGanado.setHorizontalAlignment(SwingConstants.CENTER);
		hasGanado.setBounds(42, 0, 232, 44);
		hasGanado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPanel.add(hasGanado);
		
		labelMovimientos = new JLabel("Cantidad de Movimientos:");
		labelMovimientos.setForeground(new Color(0, 0, 0));
		labelMovimientos.setBackground(Color.WHITE);
		labelMovimientos.setFont(new Font("Verdana", Font.BOLD, 13));
		labelMovimientos.setBounds(10, 78, 199, 14);
		contentPanel.add(labelMovimientos);
		
		cantidadMovimientos = new JLabel("0");
		cantidadMovimientos.setForeground(Color.BLACK);
		cantidadMovimientos.setFont(new Font("Verdana", Font.BOLD, 17));
		cantidadMovimientos.setHorizontalAlignment(SwingConstants.CENTER);
		cantidadMovimientos.setBounds(258, 72, 56, 26);
		contentPanel.add(cantidadMovimientos);
		
		panelFondo = new ImagePanel(new ImageIcon("Imagenes\\ventana\\fondoGanador.jpg").getImage());
		
		aceptar = new JButton("Aceptar");
		aceptar.setFont(new Font("Tahoma", Font.BOLD, 11));
		aceptar.setSize(97, 23);
		contentPanel.add(aceptar);
		aceptar.setLocation(112, 123);
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		aceptar.setActionCommand("OK");
		getRootPane().setDefaultButton(aceptar);
		contentPanel.add(panelFondo);
		}
	
	public void setCantidadMovimientosFinales(String movimientosFinales) 
	{
		cantidadMovimientos.setText(movimientosFinales);
	}
}
