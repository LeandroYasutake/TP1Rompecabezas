package Presentacion;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaAyuda extends JDialog 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private ImagePanel panelFondo;
	private JButton aceptar;
	private JLabel reglas;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		try 
		{
			VentanaAyuda dialog = new VentanaAyuda();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VentanaAyuda() 
	{
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		inicializar();
	}

	private void inicializar() 
	{
		setBounds(100, 100, 576, 258);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setLocationRelativeTo(null);
		contentPanel.setLayout(null);
		
		reglas = new JLabel();
		reglas.setBounds(2, 0, 576, 190);
		reglas.setIcon(new ImageIcon(MainForm.class.getResource("/ventana/reglas.png")));
		contentPanel.add(reglas);
		
		panelFondo = new ImagePanel(new ImageIcon("Imagenes\\ventana\\fondoGrande.jpg").getImage());
		
		aceptar = new JButton("Aceptar");
		aceptar.setFont(new Font("Tahoma", Font.BOLD, 11));
		aceptar.setSize(97, 23);
		contentPanel.add(aceptar);
		aceptar.setLocation(240, 195);
		aceptar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
		aceptar.setActionCommand("OK");
		getRootPane().setDefaultButton(aceptar);
		contentPanel.add(panelFondo);
		}
}
