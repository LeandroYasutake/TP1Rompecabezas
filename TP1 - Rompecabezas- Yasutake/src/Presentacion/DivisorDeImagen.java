package Presentacion;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class DivisorDeImagen 
{

	public static BufferedImage[] dividirImagen(int columnasDeseadas, int filasDeseadas, String imagen�) throws IOException 
	{
		File imagen = new File("Imagenes\\imagen"+imagen�+"\\"+imagen�+".jpeg"); 
        FileInputStream fis = new FileInputStream(imagen);
        BufferedImage image = ImageIO.read(fis); 
        
        int filas = filasDeseadas; 
        int columnas = columnasDeseadas;
        
        int casillas = filas * columnas;
        int anchoCasilla = 55; 
        int alturaCasilla = 55;
        
        if (columnasDeseadas == 4 && filasDeseadas == 4) //dispongo el tama�o del icono del boton segun el juego
        {
        	anchoCasilla = 69; 
            alturaCasilla = 69;
        }
        	
        else if (columnasDeseadas == 3 && filasDeseadas == 3)
        {
        	anchoCasilla = 92; 
            alturaCasilla = 92;
        }
        
        if (Integer.parseInt(imagen�) == 5) 
        	image = cuadrarImagen(image, 276);
       
        int posicion = 0;
        
        BufferedImage imagenes[] = new BufferedImage[casillas]; //arreglo para las casillas
       
        for (int fila = 0; fila < filas; fila++) 
        {
            for (int columna = 0; columna < columnas; columna++) 
            {
            	imagenes[posicion] = new BufferedImage(anchoCasilla, alturaCasilla, image.getType());	
                Graphics2D gr = imagenes[posicion++].createGraphics();									// dibujo la imagen de la casilla
                gr.drawImage(image, 0, 0, anchoCasilla, alturaCasilla, anchoCasilla * columna, alturaCasilla * fila, anchoCasilla * columna + anchoCasilla, alturaCasilla * fila + alturaCasilla, null);
                gr.dispose();
            }
        }
        
        return imagenes;
	}
	
	public static BufferedImage cuadrarImagen(BufferedImage image, int tama�o) 		//cuadra la imagen elegida por el usuario
	{
		BufferedImage imagenCuadrada = new BufferedImage(tama�o, tama�o, image.getType());
		Graphics2D g = imagenCuadrada.createGraphics();
		g.drawImage(image, 0, 0, 276, 276, null);
		g.dispose();
		return imagenCuadrada;
	}
}	

