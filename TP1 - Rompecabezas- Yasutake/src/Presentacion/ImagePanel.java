package Presentacion;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class ImagePanel extends JPanel 
		{
	
		/**
		 * 
	 	*/
		private static final long serialVersionUID = 1L;
		private Image imagen;

		public ImagePanel(Image imagenFondo) 
		{
			this.imagen = imagenFondo;
		    Dimension size = new Dimension(imagenFondo.getWidth(null), imagenFondo.getHeight(null));
		    setPreferredSize(size);
		    setMinimumSize(size);
		    setMaximumSize(size);
		    setSize(size);
		    setLayout(null);
		  }
		
		  @Override
		  public void paintComponent(Graphics g) 
		  {
		    g.drawImage(imagen, 0, 0, null);
		  }
}
