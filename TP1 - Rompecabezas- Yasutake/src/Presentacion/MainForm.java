package Presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logica.Juego;
import Logica.MiBoton;

public class MainForm extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Juego juego;	
	private JMenuBar menuBar;
	private JMenu juegoBoton;
	private JMenu opciones;
	private JMenu seleccionarImagen;
	private JMenu ayudaMenu;
	private JLabel movimientos;
	private JLabel numeroDeMovimientos;
	private JLabel highScore;
	private JLabel highScoreActualizable;
	private JLabel ayuda;
	private VentanaGanadora popUpGanador;
	private JMenuItem nuevoJuego25;
	private JMenuItem nuevoJuego16;
	private JMenuItem nuevoJuego9;
	private JMenuItem buscarImagen;
	private JMenuItem usarMiImagen;
	private JMenuItem imagen1;
	private JMenuItem imagen2;
	private JMenuItem numeros;
	private JMenuItem salir;
	private JMenuItem reglas;
	private JMenuItem acercaDe;
	private ImagePanel panelFondo;
	private JButton teclado;
	private VentanaAyuda ayudaReglas;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try{ 						//damos al buscador la apariencia del sistema usado
				UIManager.setLookAndFeel(
				UIManager.getSystemLookAndFeelClassName());
				}
						catch(Exception e){}
			}
		});
	}
	
	public MainForm() 
	{
		iniciarJuego();
		setearPantalla();
	}
	
	private void iniciarJuego() 
	{
		juego = new Juego();
	}
	
	private void setearPantalla() 
	{
		contentPane = new JPanel();
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		setLocationRelativeTo(null);
		contentPane.requestFocusInWindow();
		contentPane.setLayout(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		juegoBoton = new JMenu("Juego");
		menuBar.add(juegoBoton);
		
		nuevoJuego25 = new JMenuItem("Nuevo Juego 5x5");
		nuevoJuego25.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		nuevoJuego25.addActionListener(this);
		juegoBoton.add(nuevoJuego25);
		
		nuevoJuego16 = new JMenuItem("Nuevo Juego 4x4");
		nuevoJuego16.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		nuevoJuego16.addActionListener(this);
		juegoBoton.add(nuevoJuego16);
		
		nuevoJuego9 = new JMenuItem("Nuevo Juego 3x3");
		nuevoJuego9.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/webkit/prism/resources/mediaPlayDisabled.png")));
		nuevoJuego9.addActionListener(this);
		juegoBoton.add(nuevoJuego9);
		
		salir = new JMenuItem("Salir");
		salir.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/close.gif")));
		salir.addActionListener(new ActionListener() 
		{	
	        public void actionPerformed(ActionEvent ae) 
	        {
	            System.exit(0);
	        }
		});
		juegoBoton.add(salir);
		
		opciones = new JMenu("Opciones");
		menuBar.add(opciones);
		
		numeros = new JMenuItem("N\u00FAmeros");
		numeros.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/scene/web/skin/OrderedListNumbers_16x16_JFX.png")));
		numeros.addActionListener(this);
		opciones.add(numeros);
		
		imagen1 = new JMenuItem("Imagen 1");
		imagen1.setIcon(new ImageIcon("Imagenes\\iconos\\homer.jpeg"));
		imagen1.addActionListener(this);
		opciones.add(imagen1);
		
		imagen2 = new JMenuItem("Imagen 2");
		imagen2.setIcon(new ImageIcon("Imagenes\\iconos\\tierra.jpg"));
		imagen2.addActionListener(this);
		opciones.add(imagen2);
		
		seleccionarImagen = new JMenu("Seleccionar Imagen");
		menuBar.add(seleccionarImagen);
		
		buscarImagen = new JMenuItem("Buscar...");
		buscarImagen.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/directory.gif")));
		buscarImagen.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent ae) 
            {
                JFileChooser fc = new JFileChooser();
                int result = fc.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) 
                {
                    File file = fc.getSelectedFile();
                    try 
                    {
                    	BufferedImage imagen = ImageIO.read(file); 
                    	ImageIO.write(imagen, "jpeg", new File("Imagenes\\imagen5\\5.jpeg\\"));
                    } catch (Exception e) {
                    	JOptionPane.showMessageDialog(null, "Elija una imagen correcta por favor");
                    }
                }
            }
        });
		seleccionarImagen.add(buscarImagen);
		
		usarMiImagen = new JMenuItem("Usar mi imagen");
		usarMiImagen.setIcon(new ImageIcon(MainForm.class.getResource("/javax/swing/plaf/metal/icons/ocean/iconify-pressed.gif")));
		usarMiImagen.addActionListener(this);
		seleccionarImagen.add(usarMiImagen);
		
		ayudaMenu = new JMenu("Ayuda");
		menuBar.add(ayudaMenu);
		
		reglas = new JMenuItem("Reglas");
		reglas.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/java/swing/plaf/motif/icons/Question.gif")));
		reglas.addActionListener(this);
		ayudaMenu.add(reglas);
		
		acercaDe = new JMenuItem("Acerca de...");
		acercaDe.setIcon(new ImageIcon(MainForm.class.getResource("/com/sun/javafx/webkit/prism/resources/mediaTimeThumb.png")));
		acercaDe.addActionListener(this);
		ayudaMenu.add(acercaDe);
		
		movimientos = new JLabel();
		movimientos.setBounds(444, 11, 140, 35);
		ImageIcon imagenMovimientos = new ImageIcon ("Imagenes\\ventana\\Movimientos.png");
		movimientos.setIcon(imagenMovimientos);
		contentPane.add(movimientos);
		
		numeroDeMovimientos = new JLabel("0");
		numeroDeMovimientos.setBounds(474, 56, 81, 34);
		numeroDeMovimientos.setForeground(Color.WHITE);
		numeroDeMovimientos.setFont(new Font("Tahoma", Font.BOLD, 18));
		numeroDeMovimientos.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(numeroDeMovimientos);
		
		ayuda = new JLabel();
		ayuda.setBounds(455, 225, 115, 115);
		contentPane.add(ayuda);
		
		highScore = new JLabel();
		ImageIcon imagenHighscore = new ImageIcon ("Imagenes\\ventana\\highscore.png");
		highScore.setIcon(imagenHighscore);
		highScore.setBounds(444, 112, 140, 37);
		contentPane.add(highScore);
		
		highScoreActualizable = new JLabel();
		highScoreActualizable.setForeground(Color.DARK_GRAY);
		highScoreActualizable.setBackground(Color.WHITE);
		highScoreActualizable.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		highScoreActualizable.setHorizontalAlignment(SwingConstants.CENTER);
		highScoreActualizable.setBounds(491, 156, 46, 26);
		contentPane.add(highScoreActualizable);
		
		teclado = new JButton();
		teclado.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyPressed(KeyEvent e) 
			{
				if(e.getKeyCode()==KeyEvent.VK_LEFT)
				{
					juego.probaIzquierda(0, true);
				}
				else if(e.getKeyCode()==KeyEvent.VK_RIGHT)
				{
					juego.probaDerecha(0, true);
				}
				else if(e.getKeyCode()==KeyEvent.VK_UP)
				{
					juego.probaArriba(0, true);
				}
				else if(e.getKeyCode()==KeyEvent.VK_DOWN)
				{
					juego.probaAbajo(0, true);
				}
				numeroDeMovimientos.setText(Integer.toString(juego.getCantMovimientos()));
				manejarVictoria();
			}
		});
		contentPane.add(teclado);
		
		agregarBotones();
		juego.tablero.reposicionarCasilleroVacio(16); //juego por defecto 4x4
		setearBotones(); 
		
		panelFondo = new ImagePanel(new ImageIcon("Imagenes\\ventana\\Nubes.jpg").getImage());
		contentPane.add(panelFondo);
		
		popUpGanador = new VentanaGanadora();
		ayudaReglas = new VentanaAyuda();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == nuevoJuego25) 
		{
			juego.iniciarOtroJuego(25);
			retornarBotonesPanel(0); 
			resetearVentana();
		}
		
		if (e.getSource() == nuevoJuego16) 		 //inicia el juego deseado retirando o agregando
		{										//los botones a utilizar
			juego.iniciarOtroJuego(16);
			retornarBotonesPanel(9); 
			eliminarBotonesPanel(16);
			resetearVentana();
		}
		
		if (e.getSource() == nuevoJuego9) 
		{
			juego.iniciarOtroJuego(9);
			eliminarBotonesPanel(9);
			resetearVentana();
		}
		
		if (e.getSource() == reglas) 
		{
			ayudaReglas.setVisible(true);
		}
		
		if (e.getSource() == acercaDe) 
		{
			mostrarAcercaDe();
		}
		
		if (juego.getTipoDeJuego() == 25)
		{
			
			for (int posicion = 0 ; posicion < 25 ; posicion++) 
			{
				if (e.getSource() ==  juego.getBotones()[posicion]) //intento mover el boton en esa posicion del arreglo
					juego.MoverBoton(posicion);
			}
		}
		
		if (juego.getTipoDeJuego() == 16)
		{
			
			for (int posicion = 0 ; posicion < 16 ; posicion++) 
			{
				if (e.getSource() ==  juego.getBotones()[posicion])  
					juego.MoverBoton(posicion);
			}
		}
		
		if (juego.getTipoDeJuego() == 9) 
		{
			
			for (int posicion = 0 ; posicion < 9 ; posicion++) 
			{
				if (e.getSource() ==  juego.getBotones()[posicion]) 
					juego.MoverBoton(posicion);
			}
		}
		
		if (e.getSource() == numeros && !juego.isJuegoFinalizado())			//setea la imagen deseada a los botones
		{
			try {
					swapImagenes(1);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			ayuda.setIcon(null);
		}
		
		if (e.getSource() == imagen1 && !juego.isJuegoFinalizado()) 
		{
			try {
					swapImagenes(2);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			ayuda.setIcon(new ImageIcon(MainForm.class.getResource("/iconos/homerPic.png")));
		}
		
		if (e.getSource() == imagen2 && !juego.isJuegoFinalizado()) 
		{
			try {
				swapImagenes(3);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			ayuda.setIcon(new ImageIcon(MainForm.class.getResource("/iconos/tierraPic.png")));
		}
		
		if (e.getSource() == usarMiImagen && !juego.isJuegoFinalizado()) 
		{
			try {
				swapImagenes(4);
			} catch (IOException e1) {
				String nl = System.getProperty("line.separator");
				String linea1 = ("        \u00A1No se ha seleccionado una imagen!\r\n");
				String linea2 = ("Por favor, eliga una imagen de su preferencia");
				JOptionPane.showMessageDialog(null, linea1+nl+linea2);
			}
			ayuda.setIcon(null);
		}
		
		numeroDeMovimientos.setText(Integer.toString(juego.getCantMovimientos()));
		manejarVictoria();
	}

	private void agregarBotones()		//agrega los botones a la ventana
	{
		for (MiBoton boton : juego.getBotones())
		{
			contentPane.add(boton);
			boton.addActionListener(this);
		}
	}
		
	private void setearBotones() 		//coloca los botones segun la ubicacion deseada en la ventana con sus imagenes
	{
		int columna = 0;
		int ejeX = 68;
		int ejeY = 39;
		
		try {
			if (juego.getTipoDeJuego() == 25)
				cargarImagen(5, 5, Integer.toString(6));  //cargo por defecto imagen de los numeros
			else if (juego.getTipoDeJuego() == 16)
				cargarImagen(4, 4, Integer.toString(1));
			else
				cargarImagen(3, 3, Integer.toString(4));
				
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (int posBoton = 0 ; posBoton < juego.getTipoDeJuego() ; posBoton++) 
		{
			if (juego.getTipoDeJuego() == 25) 
			{
				if (columna == 5) 
				{
					columna = 0;
					ejeX = 68;
					ejeY += 56;
				}
				juego.getBotones()[posBoton].setBounds(ejeX, ejeY, 55, 55);
				columna++;
				ejeX += 57;
			}
			
			else if (juego.getTipoDeJuego() == 16) 
			{
				if (columna == 4) 
				{
					columna = 0;
					ejeX = 68;
					ejeY += 70;
				}
				juego.getBotones()[posBoton].setBounds(ejeX, ejeY, 69, 69);
				columna++;
				ejeX += 71;
			}
			
			else 
			{
				if (columna == 3) 
				{
					columna = 0;
					ejeX = 68;
					ejeY += 93;
				}
				juego.getBotones()[posBoton].setBounds(ejeX, ejeY, 92, 92);
				columna++;
				ejeX += 95;
			}
			juego.mezclarBotones();
			juego.asegurarSolucion();
		}
	}
	private void manejarVictoria()		//controla y muestra la victoria
	{
		juego.controlarVictoria();
		if (juego.isJuegoFinalizado() && !popUpGanador.getYaSeMostro()) 
		{
			highScoreActualizable.setText(juego.damePuntajeAlto());
			popUpGanador.setCantidadMovimientosFinales(Integer.toString(juego.getCantMovimientos()));
			popUpGanador.setYaSeMostro(true);
			popUpGanador.setVisible(true);
		}
	}
	
	private void swapImagenes(int imagen) throws IOException  //carga las imagenes deseadas de acuerdo al tipo de juego 
	{
		if (imagen == 1) 
		{
			if (juego.getTipoDeJuego() == 25)
				cargarImagen(5, 5, Integer.toString(6)); 
			else if (juego.getTipoDeJuego() == 16)
				cargarImagen(4, 4, Integer.toString(1));
			else
				cargarImagen(3, 3, Integer.toString(4));
		}
		
		else if (imagen == 2) 
		{
			if (juego.getTipoDeJuego() == 25)
				cargarImagen(5, 5, Integer.toString(2));
			else if (juego.getTipoDeJuego() == 16)
				cargarImagen(4, 4, Integer.toString(2));
			else
				cargarImagen(3, 3, Integer.toString(2));
		}
		
		else if (imagen == 3) 
		{
			if (juego.getTipoDeJuego() == 25)
				cargarImagen(5, 5, Integer.toString(3));
			else if (juego.getTipoDeJuego() == 16)
				cargarImagen(4, 4, Integer.toString(3));
			else
				cargarImagen(3, 3, Integer.toString(3));
		}
		
		else 
		{
			if (juego.getTipoDeJuego() == 25)
				cargarImagen(5, 5, Integer.toString(5));
			else if (juego.getTipoDeJuego() == 16)
				cargarImagen(4, 4, Integer.toString(5));
			else
				cargarImagen(3, 3, Integer.toString(5));
		}
	}
	
	private void cargarImagen(int columnasDeseadas, int filasDeseadas, String imagen�) throws IOException		//coloca a los botones las imagenes
	{
		BufferedImage[] imagenes = DivisorDeImagen.dividirImagen(columnasDeseadas, filasDeseadas, imagen�);
		
		for (MiBoton boton : juego.getBotones()) 
		{
			for (int posicion = 0 ; posicion < imagenes.length ; posicion++)
			{
				if (boton.getCasillero()-1 == posicion) 
				{
					ImageIcon icono = new ImageIcon(imagenes[posicion]);
					boton.setIcon(icono);
				}
			}
		}
	}
	
	private void eliminarBotonesPanel(int desde)		//saca los botones de juego a no utilizar
	{
		for (int boton� = desde ; boton� < juego.getBotones().length ; boton�++)
		{
			contentPane.remove(juego.getBotones()[boton�]);
		}
	}
	
	private void retornarBotonesPanel(int desde) 		//retorna los botones a juego 
	{
		for (int boton� = desde ; boton� < juego.getBotones().length ; boton�++)
		{
			contentPane.add(juego.getBotones()[boton�]);
		}
	}
	
	private void resetearVentana()		//inicializo la ventana con cada nuevo juego
	{
		setearBotones();
		popUpGanador.setVisible(false); 		//por si el usuario juega otro juego sin cerrar ventana ganadora
		popUpGanador.setYaSeMostro(false);
		numeroDeMovimientos.setText("0");
		highScoreActualizable.setText(juego.damePuntajeAlto()); 	//muestro el highscore del tipo de juego
		ayuda.setIcon(null);
		contentPane.add(panelFondo);
	}
	
	private void mostrarAcercaDe() 
	{
		String nl = System.getProperty("line.separator");
		String linea1 = ("Juego realizado por Leandro Yasutake para Programaci�n III\r\n");
		String linea2 = ("          Universidad Nacional de General Sarmiento\r\n");
		String linea3 = ("                          Rompecabezas deslizante\r\n");
		JOptionPane.showMessageDialog(null, linea1+nl+linea2+nl+linea3+nl);
	}
}