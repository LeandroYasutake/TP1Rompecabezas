package Datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class Puntajes
{
	public static void generarArchivoDePuntaje()
	{
		try{
			PrintWriter puntajes = new PrintWriter("Datos\\16.txt", "UTF-8");
			PrintWriter puntajes2 = new PrintWriter("Datos\\9.txt", "UTF-8");
			PrintWriter puntajes3 = new PrintWriter("Datos\\25.txt", "UTF-8");
		    puntajes.close();
		    puntajes2.close();
		    puntajes3.close();
		} catch (IOException e) {}
	}
	
	public static void setPuntajeAlto(int puntos, int tipoDeJuego)
	{
		if (getPuntajeAlto(tipoDeJuego) != null) 
		{
			int puntajeExistente = Integer.parseInt(getPuntajeAlto(tipoDeJuego));
			if (puntos < puntajeExistente)
			{
				try 
				{
		            FileWriter writer = new FileWriter("Datos\\"+tipoDeJuego+".txt");
		            BufferedWriter bufferedWriter = new BufferedWriter(writer);
		 
		            bufferedWriter.write(String.valueOf(puntos));
		            bufferedWriter.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
			}
		}
		
		else   //primer  juego
		{
			try 
			{
	            FileWriter writer = new FileWriter("Datos\\"+tipoDeJuego+".txt");
	            BufferedWriter bufferedWriter = new BufferedWriter(writer);
	 
	            bufferedWriter.write(String.valueOf(puntos));
	            bufferedWriter.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
	} 
	
	public static String getPuntajeAlto(int tipoDeJuego)
	{
		String puntaje = "";
		try 
		{
            FileReader reader = new FileReader("Datos\\"+tipoDeJuego+".txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            
            puntaje = bufferedReader.readLine();
            reader.close();
        } catch (IOException e) {
            return puntaje;
        }
		return puntaje;
	}
}
