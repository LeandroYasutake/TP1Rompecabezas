package Logica;

import Datos.Puntajes;

public class Juego
{
	public int CantMovimientos;
	public Tablero tablero;
	public boolean juegoFinalizado;
	public int tipoDeJuego;
	
	public Juego() 
	{
		this.tipoDeJuego = 16;  				//puede ser 16 (4x4) o 9 (3x3), primer juego 4x4 por defecto
		this.CantMovimientos = 0;
		this.tablero = new Tablero();
		this.juegoFinalizado = false;
		Puntajes.generarArchivoDePuntaje();		//genero mis archivos donde almaceno mis puntajes maximos
	}
	
	public int getTipoDeJuego() 
	{
		return this.tipoDeJuego;
	}

	public void setTipoDeJuego(int tipoDeJuego) 
	{
		this.tipoDeJuego = tipoDeJuego;
	}

	public boolean isJuegoFinalizado() 
	{
		return juegoFinalizado;
	}

	public void setJuegoFinalizado(boolean terminoJuego) 
	{
		this.juegoFinalizado = terminoJuego;
	}

	public int getCantMovimientos() 
	{
		return CantMovimientos;
	}
	
	public void setCantMovimientos(int cantMovimientos) 
	{
		CantMovimientos = cantMovimientos;
	}

	public MiBoton[] getBotones() 
	{
		return tablero.getBotones();
	}
	
	public void iniciarOtroJuego(int tipoDeJuego)		//inicia un juego nuevo
	{
		setJuegoFinalizado(false);
		setCantMovimientos(0);
		setTipoDeJuego(tipoDeJuego);
		tablero.reiniciarTablero(tipoDeJuego);
		tablero.habilitarBotones();
	}

	public void asegurarSolucion()		//evita partidas sin solucion
	{
		while (tablero.buscarParidad(this.tipoDeJuego) % 2 != 0) mezclarBotones(); 
	}
	
	public void mezclarBotones()		//mezcla los botones visibles
	{
		for (int posicion = 0 ; posicion < this.tipoDeJuego ; posicion++) 
		{
			int posicionRandom = (int)(Math.random()*this.tipoDeJuego);
			if (getBotones()[posicion].isVisible() && getBotones()[posicionRandom].isVisible())  //no mezclo la pieza vacia
			{
				tablero.swapPosiciones(getBotones()[posicion], getBotones()[posicionRandom]);
			}		
		}
	}
	
	public void controlarVictoria()		 //informa si los botones estan en sus posiciones correspondientes
	{									//o sea, si se gano el juego
		int cont = 0;
		
		for (int posicion = 0 ; posicion < this.tipoDeJuego ; posicion++) 
		{
			if (tablero.estaUbicado(getBotones()[posicion], posicion+1)) 
				cont++;
		}
		
		if (cont == this.tipoDeJuego)
		{
			terminarJuego();
		}	
	}
	
	private void terminarJuego()		//finalizo el juego grabando los puntajes
	{
		setJuegoFinalizado(true);
		tablero.apagarBotones();
		Puntajes.setPuntajeAlto(getCantMovimientos(), this.tipoDeJuego);
	}
	
	public String damePuntajeAlto()		//informa el puntaje mas alto segun el tipo de juego
	{
		return Puntajes.getPuntajeAlto(this.tipoDeJuego);
	}
	
	public void MoverBoton(int posicionBoton)		    //intenta mover el boton ubicado en la posicion dada del arreglo
	{													//si se uso el mouse
		probaArriba(posicionBoton, false);
		probaAbajo(posicionBoton, false);
		probaIzquierda(posicionBoton, false);				
		probaDerecha(posicionBoton, false);
	}
	
	public void probaArriba(int posicionBoton, boolean esTeclado)			 //se intenta mover el boton dado en cada eje
	{																		//segun el tipo de juego o medio usado
		if (this.tipoDeJuego == 25) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() > 4)
				moverAbajo(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton > 4 && !getBotones()[posicionBoton-5].isVisible())		//cambia la condicion de movimiento si es con teclado o no
				moverArriba(posicionBoton-5);
		}
		
		else if (this.tipoDeJuego == 16) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() > 3)		 						 					 //evito las posiciones borde segun el caso														
				moverAbajo(tablero.posicionCasilleroVacio());								//de ser posible, intercambio los botones, visible y no visible
			
			else if (!esTeclado && posicionBoton > 3 && !getBotones()[posicionBoton-4].isVisible())
				moverArriba(posicionBoton-4);
		}
		
		else 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() > 2)
				moverAbajo(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton > 2 && !getBotones()[posicionBoton-3].isVisible())
				moverArriba(posicionBoton-3);
		}
	}
	
	public void probaAbajo(int posicionBoton, boolean esTeclado) 
	{
		if (this.tipoDeJuego == 25) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() < 20)
				moverArriba(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton < 20 && !getBotones()[posicionBoton+5].isVisible())
				moverAbajo(posicionBoton+5);
		}
		
		else if (this.tipoDeJuego == 16) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() < 12)
				moverArriba(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton < 12 && !getBotones()[posicionBoton+4].isVisible())
				moverAbajo(posicionBoton+4);
		}
		
		else 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() < 6)
				moverArriba(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton < 6 && !getBotones()[posicionBoton+3].isVisible())
				moverAbajo(posicionBoton+3);
		}
	}

	public void probaIzquierda(int posicionBoton, boolean esTeclado) 
	{
		if (this.tipoDeJuego == 25) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() % 5 != 0)
				moverDerecha(tablero.posicionCasilleroVacio());
				
			else if (!esTeclado && posicionBoton % 5 != 0 && !getBotones()[posicionBoton-1].isVisible())
				moverIzquierda(posicionBoton-1);
		}
		
		else if (this.tipoDeJuego == 16) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() % 4 != 0)
				moverDerecha(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton % 4 != 0 && !getBotones()[posicionBoton-1].isVisible())
				moverIzquierda(posicionBoton-1);
		}
		
		else 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() % 3 != 0)
				moverDerecha(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton % 3 != 0 && !getBotones()[posicionBoton-1].isVisible())
				moverIzquierda(posicionBoton-1);
		}
	}
	
	public void probaDerecha(int posicionBoton, boolean esTeclado) 
	{
		if (this.tipoDeJuego == 25) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() % 5 != 4)
				moverIzquierda(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton % 5 != 4 && !getBotones()[posicionBoton+1].isVisible())
				moverDerecha(posicionBoton+1);
		}
		
		else if (this.tipoDeJuego == 16) 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() % 4 != 3)
				moverIzquierda(tablero.posicionCasilleroVacio());
				
			else if (!esTeclado && posicionBoton % 4 != 3 && !getBotones()[posicionBoton+1].isVisible()) 
				moverDerecha(posicionBoton+1);
		}
		
		else 
		{
			if (esTeclado && tablero.posicionCasilleroVacio() % 3 != 2)
				moverIzquierda(tablero.posicionCasilleroVacio());
			
			else if (!esTeclado && posicionBoton % 3 != 2 && !getBotones()[posicionBoton+1].isVisible()) 
				moverDerecha(posicionBoton+1);
		}
	}
	
	private void moverArriba(int posicionVacia)
	{
		int indice = 5;			//segun el juego, el casillero se haya arriba(+)/(-)abajo a 5, 4 o 3 posiciones
		
		if (this.tipoDeJuego == 16)
			indice = 4;
		else if  (this.tipoDeJuego == 9)
			indice = 3;
		
		tablero.swapBotones(getBotones()[posicionVacia+indice], getBotones()[posicionVacia]);
		this.CantMovimientos++;
	}
	
	private void moverAbajo(int posicionVacia)
	{
		int indice = 5;
		
		if (this.tipoDeJuego == 16)
			indice = 4;
		else if  (this.tipoDeJuego == 9)
			indice = 3;
	
		tablero.swapBotones(getBotones()[posicionVacia-indice], getBotones()[posicionVacia]);
		this.CantMovimientos++;
	}
	
	private void moverIzquierda(int posicionVacia)
	{
		tablero.swapBotones(getBotones()[posicionVacia+1], getBotones()[posicionVacia]);
		this.CantMovimientos++;
	}
	
	private void moverDerecha(int posicionVacia)
	{
		tablero.swapBotones(getBotones()[posicionVacia-1], getBotones()[posicionVacia]);
		this.CantMovimientos++;
	}
}	
