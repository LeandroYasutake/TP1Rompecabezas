package Logica;

import java.util.Arrays;

import javax.swing.ImageIcon;


public class Tablero {

	public MiBoton[] Botones;

	public Tablero () 
	{
		Botones = new MiBoton[25];		//permite juegos 5x5, 4x4, 3x3
		colocarBotones();
	}
	
	public MiBoton[] getBotones() {
		return Botones;
	}
	
	private void colocarBotones()		//crea el arreglo de botones guardando su n� de casillero
	{
		for (int casillero = 0 ; casillero < Botones.length ; casillero++) 
		{
			Botones[casillero] = new MiBoton(casillero+1);
		}
		Botones[Botones.length-1].setVisible(false); 		//boton a mover
	}
	
	public boolean estaUbicado(MiBoton boton, int posicion)		 //averigua si la ubicacion del boton,
	{															//coincide con su n� de casillero
		if (boton.getCasillero() == posicion) 
			return true;
		
		return false;
	}
	
	public void swapBotones(MiBoton boton1, MiBoton boton2)		//intercambia de lugar de los botones deseados
	{
		boton1.setVisible(false);
		boton2.setVisible(true);
		swapPosiciones(boton1,boton2); 
	}
	
	public void swapPosiciones(MiBoton miBoton, MiBoton miBoton2)		//intercambia de lugar los botones e imagenes
	{																   //pero sin contar su visibilidad, por ejemplo
		int posicionAux = miBoton.getCasillero();					  //para mezclarBotones()
		ImageIcon iconoAux = (ImageIcon) miBoton.getIcon();
		
		miBoton.setCasillero(miBoton2.getCasillero());
		miBoton2.setCasillero(posicionAux);
		miBoton.setIcon(miBoton2.getIcon());
		miBoton2.setIcon(iconoAux);
	}
	
	public void reiniciarTablero(int tipoDeJuego)			//sort personal (MiBoton compareTo modificado) que 
	{													   //me permite reordenar por n� de casilleros
		Arrays.sort(Botones);							  //luego reposiciono mi casillero a mover
		reposicionarCasilleroVacio(tipoDeJuego);
	}
	
	public void reposicionarCasilleroVacio(int tipoDeJuego)			 //reubica el casillero a mover (el vacio)
	{																//segun el tipo de juego
		int casilleroVacio = posicionCasilleroVacio();
		
		if (tipoDeJuego == 25 && casilleroVacio != 24)
		{
			getBotones()[24].setVisible(false);
			getBotones()[casilleroVacio].setVisible(true);
		}
		if (tipoDeJuego == 16 && casilleroVacio != 15)
		{
			getBotones()[15].setVisible(false);
			getBotones()[casilleroVacio].setVisible(true);
		}
		if (tipoDeJuego == 9 && casilleroVacio != 8)
		{
			getBotones()[8].setVisible(false);
			getBotones()[casilleroVacio].setVisible(true);
		}
	}
	
	public int posicionCasilleroVacio()				 //informa la posicion en el arreglo
	{												//del casillero movible (vacio)
		int posicionVacia = 0;
		
		for (int posicion = 1 ; posicion < Botones.length ; posicion++) 
		{
			if (!Botones[posicion].isVisible())
				posicionVacia = posicion;
		}
		return posicionVacia;
	}
	
	public void apagarBotones()					//deshabilita los botones del tablero
	{
		for (MiBoton boton : getBotones()) 
		{
			boton.setEnabled(false);
		}
	}
	
	public void habilitarBotones()			//habilita los botones del tablero
	{
		for (MiBoton boton : getBotones()) 
		{
			boton.setEnabled(true);
		}
	}
	
	public int buscarParidad(int tipoDeJuego) 		//cuento el numero de inversiones para determinar la paridad en el tablero
	{
		int paridad = 0;
	    int labelAnterior;
	    int labelSiguiente;

	    for (int posicion = 0; posicion < tipoDeJuego ; posicion++)
	    {
	        for (int posicion2 = posicion + 1; posicion2 < tipoDeJuego; posicion2++)
	        {
	        	labelAnterior = getBotones()[posicion].getCasillero();
	        	labelSiguiente = getBotones()[posicion2].getCasillero();
	            
	        	if (labelAnterior > labelSiguiente && getBotones()[posicion2].isVisible() && getBotones()[posicion].isVisible())
	            {
	                paridad++;
	            }
	        }
	    }
	    
	    return paridad;
	}
}
