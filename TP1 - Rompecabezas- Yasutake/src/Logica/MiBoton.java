package Logica;
import javax.swing.JButton;


public class MiBoton extends JButton implements Comparable<MiBoton> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int casillero;
	
	public MiBoton (int casillero) 			 //no puedo usar un setText dado que se solapan el texto con la imagen
	{										//guardo su n� de casillero en una nueva variable
		this.casillero = casillero; 
	}

	public int getCasillero() {
		return casillero;
	}

	public void setCasillero(int posicion) {
		this.casillero = posicion;
	}	
	
	@Override
    public int compareTo(MiBoton boton) 				 //implemento la interfaz comparable para ordenar 
	{													//segun mi atributo deseado, este caso por n� de casillero
        if (getCasillero() < boton.getCasillero()) 
        	{
            	return -1;
        	}
        if (getCasillero() > boton.getCasillero()) 
	        {
	            return 1;
	        }
        return 0;
    }
}
